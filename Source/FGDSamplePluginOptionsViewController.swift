//
//  FGDSamplePluginOptionsViewController.swift
//  FGDSamplePlugin
//
//  Created by Gilberto De Faveri on 23/11/15.
//  Copyright © 2015 Foreground (omnidea srl). All rights reserved.
//
//  A Swift port with some minor improvements of RMSSamplePlugin, which is part
//  of Realmac Software's official RapidWeaver plugin SDK.
//
//  =============================================================================
//  This sofware is NOT ENDORSED NOR SUPPORTED by Realmac Software in any way.
//  =============================================================================

import Foundation

class FGDSamplePluginOptionsViewController : NSViewController {
    
    required init?(representedObject: AnyObject) {
        
        super.init(nibName: "FGDSamplePluginOptionsView", bundle: FGDSamplePlugin.bundle())
        self.representedObject = representedObject
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func chooseFile(sender: AnyObject) {
        
        let openPanel = NSOpenPanel()
        openPanel.beginWithCompletionHandler { (result) -> Void in
            if result != NSFileHandlingPanelOKButton {
                return
            }
            
            let fileURL = openPanel.URL
            let pluginInstance = self.representedObject as! FGDSamplePlugin
            
            let newFileToken: String?
            do {
                newFileToken = try pluginInstance.registerFileURL(fileURL!)
            } catch let bookmarkError as NSError {
                NSLog("Failed to bookmark file with error: %@", bookmarkError)
                return
            }
            
            if (pluginInstance.fileToken != nil) {
                pluginInstance.removeFileReferenceForToken(pluginInstance.fileToken)
            }
            
            pluginInstance.fileToken = newFileToken
            
        }
        
    }
    
    @IBAction func testFileAccess(sender: AnyObject) {
        
        let pluginInstance = self.representedObject as! FGDSamplePlugin
        NSLog("fileToken: %@", pluginInstance.fileToken!)
        
        if pluginInstance.fileToken == nil {
            NSBeep()
            return
        }
        
        let fileURL: NSURL?
        do {
            fileURL = try pluginInstance.fileURLForToken(pluginInstance.fileToken)
        } catch let bookmarkError as NSError {
            NSBeep()
            NSLog("Failed to resolve token with error: %@", bookmarkError)
            return
        }
        
        if fileURL?.startAccessingSecurityScopedResource() == false {
            NSBeep()
            NSLog("Failed to enable URL access.")
            return
        }
        
        let fileData: NSData?
        do {
            fileData = try NSData(contentsOfURL: fileURL!, options: [])
        } catch let fileReadError as NSError {
            NSBeep()
            NSLog("Failed to read file with error: %@.", fileReadError)
            return
        }
        
        fileURL?.stopAccessingSecurityScopedResource()
        
        NSLog("Read data of %ld bytes.", fileData!.length)
        
    }
    
}